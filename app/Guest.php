<?php

namespace CKWedding;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table   = "guests";
    protected $guarded = ["id"];
}
